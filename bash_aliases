alias upgrade-sys="sudo apt-get -t testing update -y && time sudo apt-get -t testing upgrade -y && pipx reinstall-all"
alias nmap="sudo nmap"
alias subl="gedit"
alias luavm="luaver"
alias rustvm="rustup"
alias govm="gvm"
alias govm-update="cd ~/.goenv && git fetch --all && git pull"
alias nodevm="nvm"
alias k="kubectl"
alias gstatus="git status"
alias gadd="git add"
alias gdiff="git diff"
alias gwho="git log --graph --all --"
alias gcommit="git commit"
alias gclone="git clone"
alias gpush="git push"
alias gcheck="git checkout"
alias gbranch="git branch -a"
alias ggit="cd /media/$USER/Store/Repos"
alias gdb-record="rr record"
alias gdb-replay="rr replay"
alias ufperf="uftrace -a -t 5ns"
alias ufuser="uftrace -a --nest-libcall"
alias ufkernel="sudo uftrace -a --nest-libcall -k"
alias fclean="exifcleaner --no-sandbox"
alias 7z9="7z a -t7z -m0=lzma2 -mx=9 -myx=9 -mqs=on -ms=on"
alias 7z1="7z a -t7z -m0=lzma2 -mx=1"
alias pyremove="poetry remove"
alias pyinit="poetry init"
alias pyprod="poetry add"
alias pydev="poetry --group dev add"
alias pyactivate="poetry shell"
alias etcher="sudo balena-etcher-electron"

windows-start(){
	WINDOWS_TITLE=$(grep -i 'windows' /boot/grub/grub.cfg|grep "^[^#;]"|cut -d"'" -f2)
	sudo grub-reboot "$WINDOWS_TITLE"
	echo "Your computer will reboot on ${WINDOWS_TITLE} in 3 seconds, press Ctrl+C if you want to abord it"
	sudo reboot -d 3
}

cdebugflags="-Wall -Wextra -Werror -Wundef -Wformat=2 -Wpadded -Wconversion -Wcast-align -Wcast-qual -fstack-usage --fno-common -pedantic --function-sections --fdata-sections -pg -g3 -0g"
cprodflags="-Wall -Wextra -Wunder -Wformat=2 --function-sections --fdata-sections --gc-sections --print-gc-sections -funroll-loops"
alias cdebug="gcc -std=c17 $cdebugflags"
alias cppdebug="g++ -std=c++17 $cdebugflags"
alias cprod="gcc -std=c17 $cprodflags -Os"
alias cppprod="g++ -std=c++17 $cprodflags -O3"

pip3(){
      read -r -p "Are you sure you want to use pip3 instead of poetry [Y/n] " input
      case "$input" in
            [yY])
                  /usr/local/bin/pip3 $@
                  ;;
            [nN])
                  ;&
            *)
                  ;;
      esac
}

ufstat(){
	uftrace record $@
	uftrace report -s self
}

msgdebug(){
	case "$1" in
		"grpc")
			kreya &
			;;
		"rest")
			;&
		"graphql")
			;&
		"soap")
			insomnia &
			;;
		*)
                	echo -e "Unknown archieve format!"
	                return
        	        ;;
        esac
}
keyadd(){
        if [[ "$#" -eq 2 ]];then
                old_key_path="$1"
                user_output_file="$2"
                converted_key_name="/tmp/${user_output_file}-archive-keyring.gpg"
		download_file=${old_key_path##*/}
		wget "$old_key_path" -O /tmp/"$download_file"
                gpg --no-default-keyring --keyring /tmp/temp-keyring.gpg --import "/tmp/${download_file}"
                gpg --no-default-keyring --keyring /tmp/temp-keyring.gpg --export --output $converted_key_name
                rm /tmp/temp-keyring.gpg
		rm /tmp/"$download_file"
                sudo mv $converted_key_name /usr/share/keyrings/
        elif [[ "$#" -eq 3 ]];then
                keyserver="$1"
                fingerprint="$2"
                user_output_file="$3"
                converted_key_name="/usr/share/keyrings/${user_output_file}-archive-keyring.gpg"
                sudo gpg --no-default-keyring --keyring $converted_key_name --keyserver $keyserver --recv-keys $fingerprint
        else
                echo "Please specify original key-url and program-name"
		echo "OR"
		echo "Please specify keyserver, fingerprint and program-name"
	        return 1
        fi
	ls -la /etc/apt/sources.list.d
        echo "Edit the file /etc/apt/sources.list.d/<example>.list, and in between deb and the url, add [arch=amd64 signed-by=/usr/share/keyrings/${user_output_file}-archive-keyring.gpg]"
}
protobuild(){
	if [[ "$#" -ne 1 ]];then
		echo "Please specify a director containing protobuf's"
		return 1
	fi
    protoc --go_out="$1" --go_opt=paths=source_relative --go-grpc_out="$1" --go-grpc_opt=paths=source_relative "$1"/*.proto
}
cachecheck(){
	program_name="$1"
	perf stat -B -e cache-references,cache-misses,cycles,instructions,branches,faults,migrations $program_name
}
weather(){
	location="$1"
	curl https://wttr.in/$location
}
emailinfo(){
	email_addr="$1"
	curl emailrep.io/$email_addr
}
ipinfo(){
	ip_addr="$1"
	curl ipinfo.io/$ip_addr
	printf "\n"
	printf '\e[1;34m%-6s\e[m' " DNSLOOKUP "
	dig "$ip_addr"
	printf "\n"
	printf '\e[1;34m%-6s\e[m' " NMAP "
	nmap "$ip_addr"
}
makeroot(){
	if [[ "$#" -lt 1 ]];then
        	echo "Please specify File/Folder"
        	return 1
	fi
	sudo chgrp root "$1"

}
makemine(){
	if [[ "$#" -lt 1 ]];then
        	echo "Please specify File/Folder"
        	return 1
	fi

	sudo chown "$USER" "$@"
	sudo chgrp "$USER" "$@"
}
grefetch(){
  	if [[ "$#" -ne 1 ]];then
        	echo "Please specify deleted/corrupted file:"
	        git diff
	        return 1
  	fi
	git checkout HEAD $1
}
gpull(){
      if [[ "$#" -ne 1 ]];then
            git fetch
            git merge
      else
            git fetch origin "$1"
            git merge origin/"$1"
      fi
      git mergetool .
}
glocal(){
	if [[ "$#" -ne 1 ]];then
		echo "Please specify Branch to update:"
		git branch
		return 1
	fi
	gpull "$1"
  	git add -u
  	git commit
}
gupdate(){
	if [[ "$#" -ne 1 ]];then
		echo "Please specify Branch to update:"
		git branch
		return 1
	fi
	local branch_name="$1"
  	glocal "$branch_name"
	read -r -p "Are you sure you want to push to remote [Y/n] " input
      	case "$input" in
            [yY])
                git push -u origin "$branch_name"
                  ;;
            [nN])
                  ;&
            *)
                  ;;
     	 esac
}
gsync(){
	if [[ "$#" -ne 2 ]];then
		echo "Please specify Branch to pull NEW changes under ours: <from> <to>:"
		git branch
		return 1
	fi
	git checkout "$1"
	gpull "$1"
	git log
	git checkout "$2"
	git rebase "$1" --interactive
	git checkout "$2"
	git push -f
}
gbmove(){
	if [[ "$#" -ne 1 ]];then
		echo "Please specify Branch to move to:"
		git branch
		return 1
	fi
	git stash push
	git checkout -b "$1"
	git fetch origin "$1"
	git stash apply
	git merge origin/"$1"
	git mergetool .
	git push -u origin "$1"
	git stash drop
}
gipull(){
	git stash
	git pull
	git stash pop
}
gsyncfork(){
	if [[ "$#" -ne 2 ]];then
		echo "Please specify Upstream:"
		echo "Please specify Remote Branch:"
		git remote -v
		return 1
	fi
	git remote add upstream "$1"
	git fetch upstream
	git checkout main
	git reset --hard upstream/"$2"
	git push origin main --force
}
gmake(){
	sudo chown -R null .
	sudo chgrp -R null .

	if [[ "$#" -ne 1 ]];then
		echo "Please specify Project Name:"
		echo "SparrowOchon/<PROJECT NAME>.git"
		return 1
	fi


	if [ ! -f README.md ];then
	touch README.md
	fi
	git init
	git remote add origin git@gitlab.com:SparrowOchon/"$1".git
	git add .
	git commit -m "Initial Commit"
	git push -u origin main
}
dnschange(){
	sudo chattr -i /etc/resolv.conf
	sudo vim /etc/resolv.conf
	sudo chattr +i /etc/resolv.conf
	sudo service network-manager restart
}
extract() {

        if [[ "$#" -lt 1 ]]; then
          echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
          return 1 #not enough args
        fi

        if [[ ! -e "$1" ]]; then
          echo -e "File does not exist!"
          return 2 # File not found
        fi

        filename=$(basename "$1")
        if [[ -n "$2" ]]; then
                mkdir -p "$2" #Mkdir if doesnt exist
                DESTDIR="$2"
        else
                DESTDIR="."
                MAKEDIR=${filename%.*}
        fi
        case "${filename,,}" in
          *.tar.gz)
                echo -e "Extracting $1 to $DESTDIR: (gip compressed tar)"
                tar xvfz "$1" -C "$DESTDIR"
                ;;
          *.tgz)
                echo -e "Extracting $1 to $DESTDIR: (gip compressed tar)"
                tar xvfz "$1" -C "$DESTDIR"
                ;;
          *.tar.xz)
                echo -e "Extracting  $1 to $DESTDIR: (gip compressed tar)"
                tar xvf -J "$1" -C "$DESTDIR"
                ;;
          *.tar.bz2)
                echo -e "Extracting $1 to $DESTDIR: (bzip compressed tar)"
                tar xvfj "$1" -C "$DESTDIR"
                ;;
         *.tar)
                echo -e "Extracting $1 to $DESTDIR: (uncompressed tar)"
                tar xvf "$1" -C "$DESTDIR"
                ;;
          *.tbz2)
                echo -e "Extracting $1 to $DESTDIR: (tbz2 compressed tar)"
                tar xvjf "$1" -C "$DESTDIR"
                ;;
          *.zip)
                echo -e "Extracting $1 to $DESTDIR: (zipp compressed file)"
                unzip "$1" -d "$MAKEDIR"
                ;;
          *.lzma)
                echo -e "Extracting $1 : (lzma compressed file)"
                unlzma "$1"
                ;;
          *.rar)
                echo -e "Extracting $1 to $DESTDIR: (rar compressed file)"
                unrar x "$1" "$MAKEDIR"
                ;;
          *.7z)
                echo -e  "Extracting $1 to $DESTDIR: (7zip compressed file)"
                7za x "$1" o "$MAKEDIR"
                ;;
          *.xz)
                echo -e  "Extracting $1 : (xz compressed file)"
                unxz "$1"
                ;;
          *.bz2) echo -e  "Extracting $1 : (xz compressed file)"
                bzip2 -d "$1"
                ;;
          *.exe)
                cabextract "$1"
                ;;
          *.iso)
                echo -e  "Extracting $1 to $DESTDIR: (Iso compressed file)"
                7z x "$1"
                ;;
          *)
                echo -e "Unknown archieve format!"
                return
                ;;
        esac
}
