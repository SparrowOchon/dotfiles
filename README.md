# Linux-Configs

## Main Configs

Original Color theme: [GruvBox](https://github.com/morhetz/gruvbox)

- Tmux Config: Full Tmux config to use with the [Complete vim config](https://gitlab.com/SparrowOchon/neovim-setup) as main dev env
- Vimrc: Minimal no addon config for vim to use in servers/ssh
- Valgrind: Valgrind config for C
- bash_aliases: custom aliases for Bash shell
- terminalrc: Gruvbox theme for Xfce-terminal
- Package Control: Sublime Plugin list
- VsCodium: VsCode Plugin Setting List
- Plugin-Config: Clangformat and Golangci-lint Configs

## External Tools in Jetbrains

### Clion

- Flawfinder
- IKOS
- Semgrep (Security)

### Goland

- Golangci-lint
- Semgrep (Security)

### Pycharm

- Black
- Pylint
- Bandit
- Semgrep
- Radon
